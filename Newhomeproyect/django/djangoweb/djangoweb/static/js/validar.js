function validar() {
    var nombre, apellidos, correo, usuario, clave, telefono, expresion;
    nombre = document.getElementById("nombre").value;
    apellidos = document.getElementById("apellidos").value;
    correo = document.getElementById("correo").value;
    clave = document.getElementById("clave").value;
    telefono = document.getElementById("telefono").value;
    usuario = document.getElementById("usuario").value;
    let regNumero= new RegExp("^[0-9]+$");
    expresion = /\w+@\w+\.+[a-z]/;
    

    if (nombre === "" || apellidos === "" || correo === "" || clave === "" || telefono === "" || usuario === "") {
        alert("Todos los campos son obligatorios");
        return false;
    } else if (nombre.length > 30) {
        alert("El nombre es muy largo");
        return false;

    } else if (apellidos.length > 80) {
        alert("Los apellidos son muy largos");
        return false;

    } else if (correo.length > 100) {
        alert("El correo es muy largo");
        return false;

    } 
    else if(!expresion.test(correo)){
        alert("El correo no es valido");
        return false;
    }

    else if (clave.length > 80) {
        alert("la clave es muy larga");
        return false;

    } else if (telefono.length > 12) {
        alert("El nombre es muy largo");
        return false;

    } else if (!regNumero.test(telefono)) {
        alert("El telefono ingresado no es un numero");
        return false;

    } else if (usuario.length > 80) {
        alert("El nombre de usuario es muy largo");
        return false;

    }


}