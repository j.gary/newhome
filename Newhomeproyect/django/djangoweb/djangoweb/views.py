from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template import Template, context, loader
from AppDjCrud.forms import Clienteform
from AppDjCrud.models import Cliente
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django import forms
from django.utils.safestring import mark_safe
from django.contrib.admin.views.decorators import staff_member_required



#clases
class formusuario(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']
    username = forms.CharField(label = 'Nombre de usuario')
    password1 = forms.CharField(label='Contraseña', strip= False, widget = forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar contraseña', strip=False, widget = forms.PasswordInput)
    def __init__(self, *args, **kwargs):
        super(formusuario, self).__init__(*args, **kwargs)
        for fieldname in ['username', 'password1', 'password2']:
            self.fields[fieldname].help_text = None



def template1(request):
    temp1 = loader.get_template('index.html')
    pagina = temp1.render()
    return HttpResponse(pagina)

def template2(request):
    formreg = formusuario()
    if request.method == 'POST':
        usuarioconfirmado = formusuario(request.POST)
        if usuarioconfirmado.is_valid:
            usuarioconfirmado.save()
            return render(request, 'registro.html', {'Registro': formreg,'Mensaje': 'Usuario registrado'})
        else:
            return render(request, 'registro.html', {'Registro': formreg,'Mensaje': 'Usuario invalido'})
    return render(request, 'registro.html', {'Registro': formreg})
    

def template3(request):
    temp3 = loader.get_template('galeria.html')
    pagina = temp3.render()
    return HttpResponse(pagina)

def template4(request):
    if request.method == 'POST':
        userlog = authenticate(request, username = request.POST ['username'], password = request.POST['pass1'])
        if userlog is not None:
            auth_login(request, userlog)
            return render (request,'index.html')
        else:
            return render(request,'registro.html')
    return render(request, 'login.html')

    
def template5(request):
  
    temp5 = loader.get_template('formulariodecontact.html')
    
    
    pagina = temp5.render()
    return HttpResponse(pagina)

def template6(request):
    
    temp6 = loader.get_template('enconstruccion.html')
    
    pagina = temp6.render()
    return HttpResponse(pagina)

def template7(request):
    formuser = formusuario()
    if request.method == 'POST':
        formuser = formusuario(request.POST)
        if formuser.is_valid():
            formuser.save()
            formadmin = User.objects.get(username = request.POST['username'])
            formadmin.is_staff = True
            formadmin.save()
            return render(request, 'adminreg.html', {'RegistroAdmin': formuser, 'mensajeAdmin': 'Admin Registrado'})
        else:
            return render(request, 'adminreg.html', {'RegistroAdmin': formuser, 'mensajeAdmin': 'Admin Invalido'})
    return render(request, 'adminreg.html', {'RegistroAdmin': formuser})
       
def paglogout(request):
    logout(request)
    return render(request, 'index.html')

@staff_member_required(login_url='template1')
def cliente(request):
    l_clientes = User.objects.all()
    if request.method == "POST":
        form = Clienteform(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/clientes')
            except:
                pass
    else:
        form = Clienteform()

    return render(request,'form.html',{'form': form, 'clientes': l_clientes})

@staff_member_required(login_url='template1')
def editar(request,id):
    cli = User.objects.get(id=id)
    form = Clienteform(instance=cli)
    return render(request, 'edit.html',{'form': form, 'id': cli.id})

@staff_member_required(login_url='template1')
def modificar(request,id):
    cli = User.objects.get(id=id)
    if request.method == "POST":
        cli.username = request.POST['username']
        cli.email = request.POST['email']
        cli.save()
        return redirect('/clientes')

@staff_member_required(login_url='template1')
def eliminar(request,id):
    cli = User.objects.get(id=id)
    cli.delete()
    return redirect('/clientes')