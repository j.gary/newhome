"""djangoweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from djangoweb import views
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('clientes/', views.cliente, name = 'clientes'),
    path('adminregister/', views.template7, name = 'adminreg'),
    path('edit/<int:id>', views.editar),
    path('update/<int:id>', views.modificar),
    path('delete/<int:id>', views.eliminar),
    path ('inicio/', views.template1,name = 'inicio'),
    path('registro/', views.template2,name = 'registro'),
    path('galeria/', views.template3,name = 'galeria'),
    path('login/', views.template4,name = 'login'),
    path('contacto/', views.template5,name = 'contacto'),
    path('construccion/', views.template6,name = 'construccion'),
    path('salir/', views.paglogout, name = 'paglogout')
]
