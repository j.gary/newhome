from django import forms
from AppDjCrud.models import User


class Clienteform(forms.ModelForm):
    class Meta:
        model = User
        fields = "__all__"
