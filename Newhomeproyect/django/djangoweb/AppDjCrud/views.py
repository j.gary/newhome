from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template import Template, context, loader
from AppDjCrud.forms import Clienteform
from AppDjCrud.models import Cliente
from django.contrib.admin.views.decorators import staff_member_required


def template1(request):
    temp1 = loader.get_template('index.html')
    pagina = temp1.render()
    return HttpResponse(pagina)

def template2(request):
    temp2 = loader.get_template('registro.html')
    
    pagina = temp2.render()
    return HttpResponse(pagina)

def template3(request):
    temp3 = loader.get_template('galeria.html')
    pagina = temp3.render()
    return HttpResponse(pagina)

def template4(request):
    temp4 = loader.get_template('login.html')
   
    pagina = temp4.render()
    return HttpResponse(pagina)

def template5(request):
  
    temp5 = loader.get_template('formulariodecontact.html')
    
    
    pagina = temp5.render()
    return HttpResponse(pagina)

def template6(request):
    
    temp6 = loader.get_template('enconstruccion.html')
    
    pagina = temp6.render()
    return HttpResponse(pagina)


def clientes(request):
    clientes = Cliente.objects.all()
    if request.method == "POST":
        form = Clienteform(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/clientes')
            except:
                pass
    else:
        form = Clienteform()
        
    return render(request,'form.html',{'form': form, 'clientes': clientes})
        
def paglogout(request):
    logout(request)
    return redirect('template1')

@staff_member_required(login_url='template1')
def clientes(request):
    l_clientes = User.objects.all()
    if request.method == "POST":
        form = ClienteForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/clientes')
            except:
                pass
    else:
        form = ClienteForm()

    return render(request,'form.html',{'form': form, 'clientes': l_clientes})

@staff_member_required(login_url='template1')
def editar(request,id):
    cli = User.objects.get(id=id)
    form = ClienteForm(instance=cli)
    return render(request, 'edit.html',{'form': form, 'id': cli.id})

@staff_member_required(login_url='template1')
def modificar(request,id):
    cli = User.objects.get(id=id)
    if request.method == "POST":
        cli.username = request.POST['username']
        cli.email = request.POST['email']
        cli.save()
        return redirect('/clientes')

@staff_member_required(login_url='template1')
def eliminar(request,id):
    cli = User.objects.get(id=id)
    cli.delete()
    return redirect('/clientes')


